<?php 

class mahasiswa {

    public $nama;
    public $nim;
    public $alamat;
    public $prodi;

    function ambil_data($nama, $nim, $alamat, $prodi) {
        $this->nama = $nama;
        $this->nim = $nim;
        $this->alamat = $alamat;
        $this->prodi = $prodi;
    }

    function tampil() {
        // NAMA
        if(!$this->nama) {
            echo "<script>
                alert('Isi nama!')
                document.location.href = 'form.php';
                </script>";
        }
        else
            echo "NAMA : ".$this->nama."<br>";

        // NIM
        if(!$this->nim) {
            echo "<script>
                alert('Isi NIM!')
                document.location.href = 'form.php';
                </script>";
        }
        else
            echo "NIM : ".$this->nim."<br>";

        if(!$this->alamat) {
            echo "<script>
                alert('Isi Alamat!')
                document.location.href = 'form.php';
                </script>";
        }
        else
            echo "Alamat : ".$this->alamat."<br>";

        // prodi
        if($this->prodi == "A11")
            echo "Prodi : Teknik Informatika<br>";
        else if($this->prodi == "A22")
            echo "Prodi : Sistem Informasi<br>";
        else if($this->prodi == "B11")
            echo "Prodi : Akuntansi<br>";
        else if($this->prodi == "B22")
            echo "Prodi : Management<br>";
        else {
            echo "<script>
                alert('Data tidak valid!')
                document.location.href = 'form.php';
                </script>";
        }
    }
}
