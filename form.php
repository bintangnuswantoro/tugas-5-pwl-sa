<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <head>
    <title>Form Pendataan</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="shortcut icon" href="https://th.bing.com/th/id/R.9b8e8dbae546a58f591b724a1437682b?rik=cwD0s5Qk1f4aJg&riu=http%3a%2f%2f1000logos.net%2fwp-content%2fuploads%2f2018%2f03%2fLamborghini-logo.png&ehk=OwpaAhuJTFubTz28Y8CAGtuly%2bnAhwZzMLxNToxtDbo%3d&risl=&pid=ImgRaw&r=0">
    <nav class="navbar bg-light">
      <div class="container-fluid">
        <a class="navbar-brand" href="#">
          <img src="https://th.bing.com/th/id/R.9b8e8dbae546a58f591b724a1437682b?rik=cwD0s5Qk1f4aJg&riu=http%3a%2f%2f1000logos.net%2fwp-content%2fuploads%2f2018%2f03%2fLamborghini-logo.png&ehk=OwpaAhuJTFubTz28Y8CAGtuly%2bnAhwZzMLxNToxtDbo%3d&risl=&pid=ImgRaw&r=0" alt="" width="45" height="35" class="d-inline-block align-text-top">
          <h style="color:#fec107">Bintang Nuswantoro Company</h>
        </a>
      </div>
    </nav>
  </head>

<body>
  <link rel="stylesheet" href="styles.css">
  <form action="AksiMahasiswa.php" method="POST">
    <div class="wrapper">
      <div class="title">
        Form Pendataan
      </div>
      <div class="form">
        <div class="inputfield">
          <label>NIM</label>
          <input type="text" class="input" name="nim">
        </div>
        <div class="inputfield">
          <label>Nama</label>
          <input type="text" class="input" name="nama">
        </div>
        <div class="inputfield">
          <label>Alamat</label>
          <textarea name="alamat" class="textarea"></textarea><br>
        </div>
        <div class="inputfield">
          <label>Program Studi</label>
          <div class="custom_select">
            <select name="prodi">
              <option>Pilih Prodi</option>
              <option value="A11">Teknik Informatika</option>
              <option value="A22">Sistem Informasi</option>
              <option value="B11">Akuntansi</option>
              <option value="B22">Management</option>


            </select>
          </div>
        </div>
        <div class="inputfield">
          <input type="submit" value="Simpan" class="btn">
        </div>
      </div>
    </div>
  </form>
</body>

</html>